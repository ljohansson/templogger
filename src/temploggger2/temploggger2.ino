#include <OneWire.h>
#include <DallasTemperature.h>
#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"

/************************* 1-wire sensor *********************************/
#define ONEWIRE_PIN D4     // 1-wire pin

/************************* WiFi Access Point *********************************/

#define WLAN_SSID "__WIFI_SSID__"
#define WLAN_PASS "__WIFI_KEY__"

/************************* MQTT Setup *********************************/

#define MQTT_SERVER      "io.adafruit.com"  //"__SERVER__"
#define MQTT_SERVERPORT  1883               // use 8883 for SSL
#define MQTT_USERNAME    "__USERNAME__"
#define MQTT_PASSWORD    "__PASSWORD__"
#define MQTT_FEED        "__FEED__"

 
/** 1-WIRE **/
// Setup a oneWire instance to communicate with any OneWire devices
// (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONEWIRE_PIN);
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

/** MQTT **/
WiFiClient client;
Adafruit_MQTT_Client mqtt(&client, MQTT_SERVER, MQTT_SERVERPORT, MQTT_USERNAME, MQTT_PASSWORD);
Adafruit_MQTT_Publish tempPub = Adafruit_MQTT_Publish(&mqtt, MQTT_USERNAME MQTT_FEED);
void MQTT_connect();

int loopNr = 0;
 
void setup() 
{
  Serial.begin(9600);
  Serial.println("Templogger!");
  
  // Start up the library
  sensors.begin(); // IC Default 9 bit.
  // If you have troubles consider upping it 12.
  // Ups the delay giving the IC more time to process
  // the temperature measurement

  // Connect to WiFi access point.
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}
 
void loop() 
{
  // call sensors.requestTemperatures() to issue a global temperature
  // request to all devices on the bus
  Serial.println("Requesting temperatures...");
  sensors.requestTemperatures(); // Send the command to get temperatures
  Serial.println("DONE");
  float t = sensors.getTempCByIndex(0);
  // Why "byIndex"?
  // You can have more than one IC on the same bus.
 
  Serial.print(loopNr);
  Serial.print("\tTemperature: ");
  Serial.print(t);
  Serial.println(" *C ");

  sendUpdate(t);

  // Wait a few seconds between measurements.
  delay(300000);
  loopNr++;
}

void sendUpdate(float value) {
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.
  MQTT_connect();

  // Now we can publish stuff!
  Serial.print(F("Sending temp!"));
  Serial.print("...");
  if (! tempPub.publish(value)) {
    Serial.println(F("Failed"));
  } else {
    Serial.println(F("OK!"));
  }

  // ping the server to keep the mqtt connection alive
  // NOT required if you are publishing once every KEEPALIVE seconds
  if(! mqtt.ping()) {
    mqtt.disconnect();
  }
  
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}
