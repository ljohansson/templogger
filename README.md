# Templogger #

Arduino code for logging temperature and publish to a mqtt feed.

* templogger uses a DHT sensor
* templogger2 uses a DS18B20 1-wire sensor

## Dependencies ##

### DHT 11 ###

https://github.com/adafruit/Adafruit_Sensor

https://github.com/adafruit/DHT-sensor-library

### 1-wire ###

https://github.com/PaulStoffregen/OneWire

https://github.com/milesburton/Arduino-Temperature-Control-Library

### MQTT ###

https://github.com/adafruit/Adafruit_MQTT_Library